public class Validator {

    public boolean isValidEmail(String email) {
        int posAt = email.indexOf('@');
        return posAt > 0 && posAt < email.length()-1;
    }
}
