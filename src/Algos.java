public class Algos {

    private static final int MAX_PARAM = 22;

    static long factorielle(int n) {
        if(n < 0)
            throw new IllegalArgumentException("Le paramètre doit être un entier naturel. Reçu: " + n);
        if(n > MAX_PARAM)
            throw new IllegalArgumentException("Le paramètre n ne doit pas dépasser " + MAX_PARAM + ". Reçu: " + n);
        long resultat = 1;
        for (int i = 1; i <= n; i++) {
            resultat = resultat*i;
        }
        return resultat;
    }
}
