import org.junit.Test;

import static org.junit.Assert.*;

public class AlgosTest {

    @Test(expected = IllegalArgumentException.class)
    public void factorielle_deMoins1() {
        //noinspection ResultOfMethodCallIgnored
        Algos.factorielle(-1);
    }

    @Test
    public void factorielle_deMoins1_testMessage() {
        try {
            //noinspection ResultOfMethodCallIgnored
            Algos.factorielle(-1);
            fail("IllegalArgumentException attendue");
        } catch(IllegalArgumentException e) {
            assertEquals("Le paramètre doit être un entier naturel. Reçu: -1", e.getMessage());
        }
    }


    @Test
    public void factorielle_de0() {
        // Act
        long resultat = Algos.factorielle(0);
        // Assert
        assertEquals(1, resultat);
    }


    @Test
    public void factorielle_de1() {
        // Act
        long resultat = Algos.factorielle(1);
        // Assert
        assertEquals(1, resultat);
    }

    @Test
    public void factorielle_de2() {
        // Act & Assert
        assertEquals(2, Algos.factorielle(2));
    }

    @Test
    public void factorielle_de3() {
        // Act & Assert
        assertEquals(6, Algos.factorielle(3));
    }

    @Test
    public void factorielle_de15() {
        // Act & Assert
        assertEquals(1307674368000L, Algos.factorielle(15));
    }

    @Test
    public void factorielle_de20() {
        // Act & Assert
        assertEquals(2432902008176640000L, Algos.factorielle(20));
    }

    // Hypothèse: 23! est la limite calculable avec des long

    @Test(expected=IllegalArgumentException.class)
    public void factorielle_tropGrande() {
        // Act & Assert
        //noinspection ResultOfMethodCallIgnored
        Algos.factorielle(23);
    }





}