

import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertFalse;


public class ValidatorTest {

    Validator validator;

    @Before
    public void setUp() {
        // Arrange (préparation)
        validator = new Validator();
    }


    @Test
    public void isValidEmail_simple() {
        // Act (exécution)
        boolean result = validator.isValidEmail("schtroumpf@gmail.com");

        // Assert (vérification)
        assertTrue(result);
    }


    @Test
    public void isValidEmail_chaineVide() {
        // Act (exécution)
        boolean result = validator.isValidEmail("");

        // Assert (vérification)
        assertFalse(result);
    }

    @Test
    public void isValidEmail_arobaseSeul() {
        assertFalse(validator.isValidEmail("@"));
    }

    @Test
    public void isValidEmail_arobasePlusDomaine() {
        assertFalse(validator.isValidEmail("@gmail.com"));
    }

    @Test
    public void isValidEmail_domainePlusArobase() {
        assertFalse(validator.isValidEmail("schtroumpf@"));
    }



}